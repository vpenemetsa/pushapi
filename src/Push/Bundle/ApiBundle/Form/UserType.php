<?php

namespace Push\Bundle\ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('phonenumber')
            ->add('password')
            ->add('username')
            ->add('factions', null, array(
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Push\Bundle\ApiBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'push_bundle_apibundle_usertype';
    }
}
