<?php

namespace Push\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Push\Bundle\ApiBundle\Entity\UserRepository")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phonenumber", type="string", length=255)
     */
    private $phonenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @ORM\ManyToMany(targetEntity="Faction", mappedBy="users")
     */
    private $factions;

    /**
     * @ORM\OneToMany(targetEntity="Wire", mappedBy="user")
     */
    private $wires;

    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phonenumber
     *
     * @param string $phonenumber
     * @return User
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    
        return $this;
    }

    /**
     * Get phonenumber
     *
     * @return string 
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->factions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add factions
     *
     * @param \Push\Bundle\ApiBundle\Entity\Faction $factions
     * @return User
     */
    public function addFaction(\Push\Bundle\ApiBundle\Entity\Faction $factions)
    {
        $this->factions[] = $factions;
    
        return $this;
    }

    /**
     * Remove factions
     *
     * @param \Push\Bundle\ApiBundle\Entity\Faction $factions
     */
    public function removeFaction(\Push\Bundle\ApiBundle\Entity\Faction $factions)
    {
        $this->factions->removeElement($factions);
    }

    /**
     * Get factions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFactions()
    {
        return $this->factions;
    }

    public function setFactions($factions)
    {
        $this->factions = $factions;
    }

    /**
     * Add wires
     *
     * @param \Push\Bundle\ApiBundle\Entity\Wire $wires
     * @return User
     */
    public function addWire(\Push\Bundle\ApiBundle\Entity\Wire $wires)
    {
        $this->wires[] = $wires;
    
        return $this;
    }

    /**
     * Remove wires
     *
     * @param \Push\Bundle\ApiBundle\Entity\Wire $wires
     */
    public function removeWire(\Push\Bundle\ApiBundle\Entity\Wire $wires)
    {
        $this->wires->removeElement($wires);
    }

    /**
     * Get wires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWires()
    {
        return $this->wires;
    }
}