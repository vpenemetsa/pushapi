<?php

namespace Push\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Faction
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Push\Bundle\ApiBundle\Entity\FactionRepository")
 */
class Faction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="factions")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="Wire", inversedBy="factions")
     */
    private $wires;

    public function __construct() {
        $this->wires = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Faction
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add users
     *
     * @param \Push\Bundle\ApiBundle\Entity\User $users
     * @return Faction
     */
    public function addUser(\Push\Bundle\ApiBundle\Entity\User $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param \Push\Bundle\ApiBundle\Entity\User $users
     */
    public function removeUser(\Push\Bundle\ApiBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add wires
     *
     * @param \Push\Bundle\ApiBundle\Entity\Wire $wires
     * @return Faction
     */
    public function addWire(\Push\Bundle\ApiBundle\Entity\Wire $wires)
    {
        $this->wires[] = $wires;
    
        return $this;
    }

    /**
     * Remove wires
     *
     * @param \Push\Bundle\ApiBundle\Entity\Wire $wires
     */
    public function removeWire(\Push\Bundle\ApiBundle\Entity\Wire $wires)
    {
        $this->wires->removeElement($wires);
    }

    /**
     * Get wires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWires()
    {
        return $this->wires;
    }
}