<?php

namespace Push\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Push\Bundle\ApiBundle\Entity\WireRepository")
 */
class Wire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=1500)
     */
    private $value;

    /**
     * @ORM\ManyToMany(targetEntity="Faction", mappedBy="wires")
     */
    private $faction;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="wires")
     */
    private $user;

    public function __toString() {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Wire
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->factions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add factions
     *
     * @param \Push\Bundle\ApiBundle\Entity\Faction $factions
     * @return Wire
     */
    public function addFaction(\Push\Bundle\ApiBundle\Entity\Faction $factions)
    {
        $this->faction[] = $factions;
    
        return $this;
    }

    /**
     * Remove factions
     *
     * @param \Push\Bundle\ApiBundle\Entity\Faction $factions
     */
    public function removeFaction(\Push\Bundle\ApiBundle\Entity\Faction $factions)
    {
        $this->faction->removeElement($factions);
    }

    /**
     * Get factions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction($faction)
    {
        $this->faction = $faction;
    }

    /**
     * Set user
     *
     * @param \Push\Bundle\ApiBundle\Entity\User $user
     * @return Wire
     */
    public function setUser(\Push\Bundle\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Push\Bundle\ApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}