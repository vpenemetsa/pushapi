<?php

namespace Push\Bundle\ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;
use FOS\RestBundle\View\View as FOSView;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\View;

class BaseController extends Controller
{
	/**
	 * @var ViewHandler
	 */
	protected $viewHandler;

	public function __construct(ViewHandler $viewHandler)
	{
		$this->viewHandler = $viewHandler;
	}

	public function getOneFaction($factionId) {
        return $this->getDoctrine()->getEntityManager()->getRepository('PushApiBundle:Faction')->findOneById($factionId);
    }

    public function getOneUser($userId) {
        return $this->getDoctrine()->getEntityManager()->getRepository('PushApiBundle:User')->findOneById($userId);
    }

    public function getOneWire($wireId) {
        return $this->getDoctrine()->getEntityManager()->getRepository('PushApiBundle:Wire')->findOneById($wireId);
    }
}