<?php

namespace Push\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;

use Push\Bundle\ApiBundle\Entity\Faction;
use Push\Bundle\ApiBundle\Form\FactionType;

use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

use FOS\RestBundle\View\View as FOSView;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;


/**
 * Faction controller.
 *
 */
class FactionController extends BaseController
{

    /**
     * @Route("/")
     */
    public function getFactionsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PushApiBundle:Faction')->findAll();

        $view = View::create()
            ->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'Faction', 'index'))
            ->setData($entities);

        return $this->viewHandler->handle($view);
        // return $this->render('PushApiBundle:Faction:index.html.twig', array(
        //     'entities' => $entities,
        // ));
    }

    /**
     * Creates a new Faction entity.
     * @Route("/")
     */
    public function postFactionAction(Request $request)
    {
        $faction  = new Faction();
        
        $faction->setTitle($request->request->get('title'));
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();


        // $form = $this->createForm(new FactionType(), $entity);
        // $form->bind($request);

        // if ($form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('faction_show', array('id' => $entity->getId())));
        // }
        // 
        
        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // return $this->render('PushApiBundle:Faction:new.html.twig', array(
        //     'entity' => $entity,
        //     'form'   => $form->createView(),
        // ));
    }

    /**
     * @Route("{id}", requirements={"id"})
     */
    public function getFactionAction($id)
    {
        $entity = $this->getOneFaction($id);

        $view = View::create();

        if (!$entity) {
            $view->setStatusCode(404)
                ->setTemplate(new TemplateReference('PushApiBundle', 'Faction', 'show'))
                ->setData($entity);
        } else {
            $view->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'Faction', 'show'))
            ->setData($entity);    
        }

        return $this->viewHandler->handle($view);
    }

    // /**
    //  * Displays a form to edit an existing Faction entity.
    //  *
    //  */
    // public function editAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('PushApiBundle:Faction')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Faction entity.');
    //     }

    //     $editForm = $this->createForm(new FactionType(), $entity);
    //     $deleteForm = $this->createDeleteForm($id);

    //     return $this->render('PushApiBundle:Faction:edit.html.twig', array(
    //         'entity'      => $entity,
    //         'edit_form'   => $editForm->createView(),
    //         'delete_form' => $deleteForm->createView(),
    //     ));
    // }

    /**
     * Edits an existing Faction entity.
     * @Route("{id}", requirements={"id"})
     */
    public function postPutFactionAction(Request $request, $id)
    {
        $faction = $this->getOneFaction($id);
        $faction->setTitle($request->request->get('title'));

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // $entity = $em->getRepository('PushApiBundle:Faction')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find Faction entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);
        // $editForm = $this->createForm(new FactionType(), $entity);
        // $editForm->bind($request);

        // if ($editForm->isValid()) {
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('faction_edit', array('id' => $id)));
        // }

        // return $this->render('PushApiBundle:Faction:edit.html.twig', array(
        //     'entity'      => $entity,
        //     'edit_form'   => $editForm->createView(),
        //     'delete_form' => $deleteForm->createView(),
        // ));
    }

    /**
     * Deletes a Faction entity.
     * @Route("/delete/{id}", requirements={"id"})
     */
    public function postDeleteFactionAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $this->getOneFaction($id);

        $em->remove($entity);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // $form = $this->createDeleteForm($id);
        // $form->bind($request);

        // if ($form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $entity = $em->getRepository('PushApiBundle:Faction')->find($id);

        //     if (!$entity) {
        //         throw $this->createNotFoundException('Unable to find Faction entity.');
        //     }

        //     $em->remove($entity);
        //     $em->flush();
        // }

        // return $this->redirect($this->generateUrl('faction'));
    }

    /**
     * @Route("/add/wire/{id}", requirements={"id"})
     */
    public function postAddWireAction(Request $request, $id)
    {
        $faction = $this->getOneFaction($id);
        $wire = $this->getOneWire($request->request->get('wireId'));
        $faction->addWire($wire);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($faction);

        return $this->viewHandler->handle($view);
    }

    /**
     * @Route("/delete/wire/{id}", requirements={"id"})
     */
    public function postDeleteWireAction(Request $request, $id)
    {
        $faction = $this->getOneFaction($id);
        $wire = $this->getOneWire($request->request->get('wireId'));
        $faction->removeWire($wire);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($faction);

        return $this->viewHandler->handle($view);
    }

    /**
     * @Route("/add/user/{id}", requirements={"id"})
     */
    public function postAddUserAction(Request $request, $id)
    {
        $faction = $this->getOneFaction($id);
        $user = $this->getOneUser($request->request->get('userId'));
        $faction->addUser($user);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($faction);

        return $this->viewHandler->handle($view);
    }

    /**
     * @Route("/delete/user/{id}", requirements={"id"})
     */
    public function postDeleteUserAction(Request $request, $id)
    {
        $faction = $this->getOneFaction($id);
        $user = $this->getOneUser($request->request->get('userId'));
        $faction->removeWire($user);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($faction);

        return $this->viewHandler->handle($view);
    }

    /**
     * Creates a form to delete a Faction entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
