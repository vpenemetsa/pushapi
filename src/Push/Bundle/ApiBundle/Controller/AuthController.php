<?php

namespace Push\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;

use Push\Bundle\ApiBundle\Entity\Faction;
use Push\Bundle\ApiBundle\Form\FactionType;

use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

use FOS\RestBundle\View\View as FOSView;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;


/**
 * Auth controller.
 *
 */
class AuthController extends BaseController
{

}