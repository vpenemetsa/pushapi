<?php

namespace Push\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;

use Push\Bundle\ApiBundle\Entity\User;
use Push\Bundle\ApiBundle\Form\UserType;

use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

use FOS\RestBundle\View\View as FOSView;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;

/**
 * User controller.
 *
 */
class UserController extends BaseController
{

    /**
     * @Route("/")
     */
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PushApiBundle:User')->findAll();

        $view = View::create()
            ->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'User', 'index'))
            ->setData($entities);

        return $this->viewHandler->handle($view);

        // return $this->render('PushApiBundle:User:index.html.twig', array(
        //     'entities' => $entities,
        // ));
    }

    /**
     * Creates a new User entity.
     * @Route("/")
     */
    public function postUserAction(Request $request)
    {

        $user  = new User();
        
        $user->setName($request->request->get('name'));
        $user->setUsername($request->request->get('username'));
        $user->setPassword($request->request->get('password'));
        $user->setEmail($request->request->get('email'));
        $user->setPhonenumber($request->request->get('phonenumber'));
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($user);


        return $this->viewHandler->handle($view);

        // $entity  = new User();
        // $form = $this->createForm(new UserType(), $entity);
        // $form->bind($request);

        // if ($form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        // }

        // return $this->render('PushApiBundle:User:new.html.twig', array(
        //     'entity' => $entity,
        //     'form'   => $form->createView(),
        // ));
    }

    // /**
    //  * Displays a form to create a new User entity.
    //  *
    //  */
    // public function newAction()
    // {
    //     $entity = new User();
    //     $form   = $this->createForm(new UserType(), $entity);

    //     return $this->render('PushApiBundle:User:new.html.twig', array(
    //         'entity' => $entity,
    //         'form'   => $form->createView(),
    //     ));
    // }

    /**
     * Finds and displays a User entity.
     * @Route("{id}", requirements={"id"})
     */
    public function getUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PushApiBundle:User')->find($id);

        $view = View::create();

        if (!$entity) {
            $view->setStatusCode(404)
                ->setTemplate(new TemplateReference('PushApiBundle', 'User', 'show'))
                ->setData($entity);
        } else {
            $view->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'User', 'show'))
            ->setData($entity);    
        }

        return $this->viewHandler->handle($view);


        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find User entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);

        // return $this->render('PushApiBundle:User:show.html.twig', array(
        //     'entity'      => $entity,
        //     'delete_form' => $deleteForm->createView(),        ));
    }

    // /**
    //  * Displays a form to edit an existing User entity.
    //  *
    //  */
    // public function editAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('PushApiBundle:User')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find User entity.');
    //     }

    //     $editForm = $this->createForm(new UserType(), $entity);
    //     $deleteForm = $this->createDeleteForm($id);

    //     return $this->render('PushApiBundle:User:edit.html.twig', array(
    //         'entity'      => $entity,
    //         'edit_form'   => $editForm->createView(),
    //         'delete_form' => $deleteForm->createView(),
    //     ));
    // }

    /**
     * Edits an existing User entity.
     * @Route("{id}", requirements={"id"})
     */
    public function postPutUserAction(Request $request, $id)
    {
        $user = $this->getOneUser($id);
        $name = trim($request->request->get('name'));
        $password = trim($request->request->get('password'));

        if (strlen($name) > 0) {
            $user->setName($name);    
        }
        if (strlen($password) > 0) {
            $user->setPassword();    
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // $em = $this->getDoctrine()->getManager();

        // $entity = $em->getRepository('PushApiBundle:User')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find User entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);
        // $editForm = $this->createForm(new UserType(), $entity);
        // $editForm->bind($request);

        // if ($editForm->isValid()) {
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        // }

        // return $this->render('PushApiBundle:User:edit.html.twig', array(
        //     'entity'      => $entity,
        //     'edit_form'   => $editForm->createView(),
        //     'delete_form' => $deleteForm->createView(),
        // ));
    }

    // /**
    //  * Deletes a User entity.
    //  *
    //  */
    // public function deleteAction(Request $request, $id)
    // {
    //     $form = $this->createDeleteForm($id);
    //     $form->bind($request);

    //     if ($form->isValid()) {
    //         $em = $this->getDoctrine()->getManager();
    //         $entity = $em->getRepository('PushApiBundle:User')->find($id);

    //         if (!$entity) {
    //             throw $this->createNotFoundException('Unable to find User entity.');
    //         }

    //         $em->remove($entity);
    //         $em->flush();
    //     }

    //     return $this->redirect($this->generateUrl('user'));
    // }

    /**
     * @Route("/delete/faction/{id}", requirements={"id"})
     */
    public function postDeleteFactionAction(Request $request, $id)
    {
        $user = $this->getOneUser($id);
        $faction = $this->getOneFaction($request->request->get('factionId'));
        $faction->removeUser($user);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);
    }

    /**
     * @Route("/add/faction/{id}", requirements={"id"})
     */
    public function postAddFactionAction(Request $request, $id)
    {
        $user = $this->getOneUser($id);
        $faction = $this->getOneFaction($request->request->get('factionId'));
        $faction->addUser($user);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($faction);
        $em->flush();

        $view = View::create()
            ->setStatusCode(200)
            ->setData($user);

        return $this->viewHandler->handle($view);
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
