<?php

namespace Push\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;

use Push\Bundle\ApiBundle\Entity\Wire;
use Push\Bundle\ApiBundle\Form\WireType;

use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

use FOS\RestBundle\View\View as FOSView;
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;

/**
 * Wire controller.
 *
 */
class WireController extends BaseController
{
    /**
     * @Route("/")
     */
    public function getWiresAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PushApiBundle:Wire')->findAll();

        $view = View::create()
            ->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'Wire', 'index'))
            ->setData($entities);

        return $this->viewHandler->handle($view);

        // return $this->render('PushApiBundle:Wire:index.html.twig', array(
        //     'entities' => $entities,
        // ));
    }

    /**
     * Creates a new Wire entity.
     * @Route("/")
     */
    public function postWireAction(Request $request)
    {
        $wire  = new Wire();
        
        $wire->setValue($request->request->get('message'));
        $wire->setUser($this->getOneUser($request->request->get('userId')));
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($wire);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // $entity  = new Wire();
        // $form = $this->createForm(new WireType(), $entity);
        // $form->bind($request);

        // if ($form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('wire_show', array('id' => $entity->getId())));
        // }

        // return $this->render('PushApiBundle:Wire:new.html.twig', array(
        //     'entity' => $entity,
        //     'form'   => $form->createView(),
        // ));
    }

    // /**
    //  * Displays a form to create a new Wire entity.
    //  *
    //  */
    // public function newAction()
    // {
    //     $entity = new Wire();
    //     $form   = $this->createForm(new WireType(), $entity);

    //     return $this->render('PushApiBundle:Wire:new.html.twig', array(
    //         'entity' => $entity,
    //         'form'   => $form->createView(),
    //     ));
    // }

    /**
     * Finds and displays a Wire entity.
     * @Route("{id}", requirements={"id"})
     */
    public function getWireAction($id)
    {
        $entity = getOneWire($id);

        $view = View::create();

        if (!$entity) {
            $view->setStatusCode(404)
                ->setTemplate(new TemplateReference('PushApiBundle', 'Wire', 'show'))
                ->setData($entity);
        } else {
            $view->setStatusCode(200)
            ->setTemplate(new TemplateReference('PushApiBundle', 'Wire', 'show'))
            ->setData($entity);    
        }

        return $this->viewHandler->handle($view);


        // $em = $this->getDoctrine()->getManager();

        // $entity = $em->getRepository('PushApiBundle:Wire')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find Wire entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);

        // return $this->render('PushApiBundle:Wire:show.html.twig', array(
        //     'entity'      => $entity,
        //     'delete_form' => $deleteForm->createView(),        ));
    }

    // /**
    //  * Displays a form to edit an existing Wire entity.
    //  *
    //  */
    // public function editAction($id)
    // {
    //     $em = $this->getDoctrine()->getManager();

    //     $entity = $em->getRepository('PushApiBundle:Wire')->find($id);

    //     if (!$entity) {
    //         throw $this->createNotFoundException('Unable to find Wire entity.');
    //     }

    //     $editForm = $this->createForm(new WireType(), $entity);
    //     $deleteForm = $this->createDeleteForm($id);

    //     return $this->render('PushApiBundle:Wire:edit.html.twig', array(
    //         'entity'      => $entity,
    //         'edit_form'   => $editForm->createView(),
    //         'delete_form' => $deleteForm->createView(),
    //     ));
    // }

    /**
     * Edits an existing Wire entity.
     * @Route("{id}", requirements={"id"})
     */
    public function postPutWireAction(Request $request, $id)
    {
        $wire = getOneWire($id);
        $wire->setValue($request->request->get('message'));

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($wire);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);


        // $em = $this->getDoctrine()->getManager();

        // $entity = $em->getRepository('PushApiBundle:Wire')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find Wire entity.');
        // }

        // $deleteForm = $this->createDeleteForm($id);
        // $editForm = $this->createForm(new WireType(), $entity);
        // $editForm->bind($request);

        // if ($editForm->isValid()) {
        //     $em->persist($entity);
        //     $em->flush();

        //     return $this->redirect($this->generateUrl('wire_edit', array('id' => $id)));
        // }

        // return $this->render('PushApiBundle:Wire:edit.html.twig', array(
        //     'entity'      => $entity,
        //     'edit_form'   => $editForm->createView(),
        //     'delete_form' => $deleteForm->createView(),
        // ));
    }

    /**
     * Deletes a Wire entity.
     * @Route("/delete/{id}", requirements={"id"})
     */
    public function postDeleteWireAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = getOneWire($id);

        $em->remove($entity);
        $em->flush();

        $view = View::create()->setStatusCode(200);

        return $this->viewHandler->handle($view);

        // $form = $this->createDeleteForm($id);
        // $form->bind($request);

        // if ($form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $entity = $em->getRepository('PushApiBundle:Wire')->find($id);

        //     if (!$entity) {
        //         throw $this->createNotFoundException('Unable to find Wire entity.');
        //     }

        //     $em->remove($entity);
        //     $em->flush();
        // }

        // return $this->redirect($this->generateUrl('wire'));
    }

    /**
     * Creates a form to delete a Wire entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
